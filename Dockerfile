FROM maven:latest

WORKDIR /app

COPY pom.xml .

COPY src ./src

RUN mvn clean package -DskipTests

EXPOSE 8761

CMD ["java", "-jar", "target/service-discovery-0.0.1-SNAPSHOT.jar"]
